# Documentation for [Spring Cloud Infrastructure](https://gitlab.com/spring-cloud-infrastructure)

![Spring Cloud Architecture](./diagrams/architecture.png)

# Components

- [API Gateway](https://gitlab.com/spring-cloud-infrastructure/api-gateway)
- [Service Discovery](https://gitlab.com/spring-cloud-infrastructure/service-discovery)
- [Config Server](https://gitlab.com/spring-cloud-infrastructure/config-server)
- [Spring Boot Admin](https://gitlab.com/spring-cloud-infrastructure/spring-boot-admin)
- [Authorization Server](https://gitlab.com/spring-cloud-infrastructure/authorization-server)
- [Scheduler Service](https://gitlab.com/spring-cloud-infrastructure/scheduler-service)
- [Service A](https://gitlab.com/spring-cloud-infrastructure/service-a)
- [Service B](https://gitlab.com/spring-cloud-infrastructure/service-b)
- [Docker](https://gitlab.com/spring-cloud-infrastructure/docker)
- [Config Repo](https://gitlab.com/spring-cloud-infrastructure/config-repo)
